// 2. Convert all the salary values into proper numbers instead of strings

let data = require("./1-arrays-jobs.cjs");
function salaryInNumber(data) {
  return data.map((person) => {
    person.salary = Number(person.salary.replace("$", ""));
    return person;
  });
}

module.exports = salaryInNumber;
