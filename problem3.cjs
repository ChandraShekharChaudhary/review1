// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function correctSalary(data){
    return data.map((person) => {
        let salary1= Number(person.salary.replace("$", ""))*10000;
        person['corrected_salary']=`$${salary1}`;
        return person;
      });
}
module.exports=correctSalary;