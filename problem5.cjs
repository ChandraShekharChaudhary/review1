// 5. Find the sum of all salaries based on country using only HOF method

function salaryByCountry(data){
    return data.reduce((result,employ)=>{
        let country=employ.location;
        return {...result,[country]:(result[country] || 0)+Number(employ.salary.replace("$", ''))*10000} ;
    }, {})
}
module.exports=salaryByCountry;