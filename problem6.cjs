// 6. Find the average salary of based on country using only HOF method

function averageSalaryByCountry(data) {
  let count = {};
  let averageSalary = {};
   const salary = data.reduce((result, employ) => {
    let country = employ.location;
    count[country] = (count[country] || 0) + 1;
    return {...result,[country]:(result[country] || 0) + Number(employ.salary.replace("$", "")) * 10000,
    };
  }, {});

  for (let country in salary) {
    averageSalary[country] = salary[country] / count[country];
  }
  return averageSalary;
}

module.exports=averageSalaryByCountry;